#!/bin/bash

################################################################################
# Check Point Logger for As-Built
#
# Description:
#   This script executes a series of commands on a Check Point firewall and
#   logs the output of each command to a timestamped log file. It also displays
#   the output on the terminal.
#
# Usage:
#   ./checkpoint_logger.sh
#
# Notes:
#   - This script requires elevated privileges to run certain commands.
#
#   - Ensure that the 'clish' and 'expert' modes are properly configured and
#     accessible on the Check Point firewall.
#
#   - The list of commands and their execution contexts (expert or clish) are
#     defined in the associative array 'commands'.
#
#   - The output of each command is appended to a log file named with a
#     timestamp.
#
#   - The script utilizes 'tee' to display the output on the terminal and to
#     append it to the log file simultaneously.
#
#   - The log file is stored in the same directory as the script.
#
#   - For commands executed in 'expert' mode, ensure that the environment is
#     properly set up to execute them without issues.
#
# Author:
#   Nelson Junior aka x86DX2
#
# Date:
#   10/05/2024 10:00 AM
#
# Version:
#   2024051001
#
# Repository:
#   https://gitlab.com/x86dx2/checkpoint-logger-for-asbuilt
#
################################################################################

# Associative array of commands with their respective contexts
commands_list=(
    "expert:fw ver"
    "expert:cpinfo -y all"
    "expert:cphaprob state"
    "expert:cpstat ha"
    "expert:cpstat fw"
    "expert:cpstat os -f all"
    "expert:cphaprob -a if"
    "expert:cphaprob -l list"
    "expert:cphaprob syncstat"
    "expert:cphaprob ldstat"
    "expert:cphaconf cluster_id get"
    "expert:fwaccel stat"
    "expert:fwaccel stats -s"
    "expert:fw getifs"
    "expert:fw stat -s"
    "expert:fw stat -l"
    "expert:fw tab -f -t vpn_routing -u"
    "expert:fw tab -s -t userc_users"
    "expert:enabled_blades"
    "expert:cplic print -x"
    "expert:netstat -I"
    "expert:fw ctl arp"
    "expert:fw ctl iflist"
    "clish:fw ctl affinity -l -v -r"
    "clish:show asset all"
    "clish:show sysenv all"
    "clish:show interfaces"
    "clish:show interfaces all"
    "clish:show route"
    "clish:show bgp peers"
    "clish:show bgp summary"
    "clish:show bgp stats"
    "clish:show ospf neighbors"
    "clish:show ospf neighbors detailed"
    "clish:show ospf summary"
    "clish:show ospf database"
)

# Timestamped log file name
timestamp=$(date +"%Y-%m-%d_%H-%M-%S")
log_file="output_${timestamp}.log"

# Function to print to screen and log
print_log() {
    message="$1"
    echo "$message"
    echo "$message" >>"$log_file"
}

for commands in "${commands_list[@]}"; do
    # Get the command and context
    context="${commands%%:*}"
    command="${commands#*:}"

    # Print the command being executed on the screen and in the log
    print_log "[$context]: $command"

    # Execute the command in the appropriate context and redirect the output to the screen and log,
    if [ "$context" == "expert" ]; then
        "$command" 2>&1 | tee -a "$log_file"
    else
        clish -c "$command" 2>&1 | tee -a "$log_file"
    fi
done

print_log "Check the log file at '$(realpath "$log_file")'."

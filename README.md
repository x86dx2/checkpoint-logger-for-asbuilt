# Check Point Logger for As-Built


## Description:
This script executes a series of commands on a Check Point firewall and logs the output of each command to a timestamped log file. It also displays the output on the terminal.

## Usage:
1. **Download the script:**
```bash
curl -o checkpoint_logger.sh https://gitlab.com/x86dx2/checkpoint-logger-for-asbuilt/-/raw/main/checkpoint_logger.sh

or 

wget -O checkpoint_logger.sh https://gitlab.com/x86dx2/checkpoint-logger-for-asbuilt/-/raw/main/checkpoint_logger.sh
```

2. **Grant execution permission:**
```bash
sudo chmod +x checkpoint_logger.sh
```

3. **Run the script:**
```bash
./checkpoint_logger.sh
```

## Notes:
- [ ] This script requires elevated privileges to run certain commands.
- [ ] Ensure that the 'clish' and 'expert' modes are properly configured and accessible on the Check Point firewall.
- [ ] The list of commands and their execution contexts (expert or clish) are
defined in the associative array 'commands_list'.
- [ ] The output of each command is appended to a log file named with a
timestamp.
- [ ] The script utilizes 'tee' to display the output on the terminal and to append it to the log file simultaneously.
- [ ] The log file is stored in the same directory as the script.
- [ ] For commands executed in 'expert' mode, ensure that the environment is properly set up to execute them without issues.

## Thanks 🤝
I would like to express my gratitude to two special individuals who contributed to the development of this program:

- **Eduardo Campos**: I want to thank Eduardo Campos for being a source of inspiration for this project.

- **Fábio Ferreira**: Special thanks to Fábio Ferreira for dedicating his time and effort to test this program and validate its functionality. His insights and feedback were invaluable in improving the quality and reliability of the software.

To both of them, my sincere appreciation for the support and contribution to this project.